import re
from csv import reader
from typing import Callable

import typer
from docx import Document


class Translator:
    def __init__(self, custom_dictionary: dict, from_lang: str = "uk"):
        from deep_translator import GoogleTranslator

        self.translator = GoogleTranslator(source=from_lang)
        self._custom_dictionary = custom_dictionary
        self._empty_text_re = re.compile(
            r'[\s!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~0123456789]+'
        )

    def translate(self, line: str) -> str:
        return self._translate_line(line) if self._has_text(line) else line

    def _has_text(self, line: str) -> bool:
        return len(self._empty_text_re.sub("", line)) > 1

    def _translate_line(self, line: str) -> str:
        return self._online_translation(self._custom_dictionary_translation(line))

    def _online_translation(self, line: str) -> str:
        return line
        # self.translator.translate(line)

    def _custom_dictionary_translation(self, line: str) -> str:
        result = line
        for source, translation in self._custom_dictionary.items():
            result = self._replace_all_variants(source, translation, result)
        return result

    def _replace_all_variants(self, source: str, translation: str, line: str) -> str:
        result = line
        # 'soUrce' > 'transLation'
        result = result.replace(source, translation)
        # 'source' > 'translation'
        result = result.replace(source.lower(), translation.lower())
        # 'SOURCE' > 'TRANSLATION'
        result = result.replace(source.upper(), translation.upper())
        # 'Source' > 'Translation'
        # result = result.replace(source.title(), translation.title())
        return result


class DocVisitor:
    def __init__(self, doc: Document):
        self.doc = doc

    def visit(self, func: Callable[[str], str]):
        for table in self.doc.tables:
            for row in table.rows:
                for cell in row.cells:
                    self._visit_paragraphs(cell, func)

        self._visit_paragraphs(self.doc, func)

    def _visit_paragraphs(self, p, func: Callable[[str], str]):
        for para in p.paragraphs:
            for run in para.runs:
                translation = func(run.text)
                if translation:
                    run.text = translation


def load_dictionary(file_name: str) -> dict:
    with open(file_name, mode="r") as file:
        return dict(reader(file, delimiter="\t"))


def main(source: str, target: str, dictionary: str):
    print(f"reading custom dictionary {dictionary}")
    translator = Translator(load_dictionary(dictionary), "uk")

    print(f"reading doc from {source}")
    doc = Document(source)

    visitor = DocVisitor(doc)
    visitor.visit(lambda s: translator.translate(s))

    doc.save(target)
    print(f"saved doc to {target}")


if __name__ == "__main__":
    typer.run(main)

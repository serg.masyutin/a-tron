#!/bin/zsh
# $1 -- local /data volume path
# $2 -- input file path relative to /data volume
# $3 -- output file path relative to /data volume
# $4 -- dictionary file path relative to /data volume
docker run -v "$1":/data smasyutin/a-tron:latest /data/"$2" /data/"$3" /data/"$4"